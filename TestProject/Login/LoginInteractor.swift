//
//  Interactor.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class LoginInteractor: PTILoginProtocol{
       
    var presenter: ITPLoginProtocol?
    
    func getLogin(nav: UINavigationController, username: String, password: String) {
        //hit api here
        var userList = [UserModel]()
//        let paramsAccountDetail = ["username" : "dini"]
        let url = "https://jsonplaceholder.typicode.com/users"
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
//                            print("DATA USER")
//                            print(resJSON)
                            let allUserResponse = resJSON.arrayValue
                            for item in allUserResponse {
                                userList.append(UserModel(_: item))
                            }
                            print("DATA USER")
                            print(userList)
                            for item in userList {
                                if (item.username == username) {
                                    print("USERNAME MATCH \(username)")
//                                    let profileID = String(item.id)
                                    print("USER ID MATCH \(String(item.id))")
                                    self.presenter?.loginSuccess(nav: nav, profileId: String(item.id))
                                } else {
                                    print("username doesn't match")
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    })
    }

    
    
}
