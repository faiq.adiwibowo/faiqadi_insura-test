//
//  UserModel.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import SwiftyJSON

struct UserModel {
    var id : Int = 0
    var name: String = ""
    var username: String = ""
    var email: String = ""
    var phone: String = ""
    var website: String = ""
    var address = addressDetail()
    var company = conpanyDetail()
    
    
    
    init() {}

       init(_ json: JSON) {
           if let _id = json["id"].int{
               id = _id
           }
           if let _name = json["name"].string{
               name = _name
           }
           if let _username = json["username"].string{
               username = _username
           }
           if let _email = json["email"].string{
               email = _email
           }
           if let _phone = json["phone"].string{
               phone = _phone
           }
           if let _website = json["website"].string{
               website = _website
           }
           address = addressDetail(json)
           company = conpanyDetail(json)
       }
}

struct addressDetail {
    var street: String = ""
    var suite: String = ""
    var city: String = ""
    var zipcode: String = ""
    
    init() {}

       init(_ json: JSON) {
           if let _street = json["street"].string{
               street = _street
           }
           if let _suite = json["suite"].string{
               suite = _suite
           }
           if let _city = json["city"].string{
               city = _city
           }
           if let _zipcode = json["zipcode"].string{
               zipcode = _zipcode
           }
       }
}

struct conpanyDetail {
    var name: String = ""
    var catchPhrase: String = ""
    var bs: String = ""
    
    init() {}

       init(_ json: JSON) {
           if let _name = json["name"].string{
               name = _name
           }
           if let _catchPhrase = json["catchPhrase"].string{
               catchPhrase = _catchPhrase
           }
           if let _bs = json["bs"].string{
               bs = _bs
           }
       }
}
