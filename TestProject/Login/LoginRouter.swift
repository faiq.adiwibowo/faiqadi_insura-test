//
//  LoginRouter.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import UIKit

class LoginRouter:PTRLoginProtocol{

    static func createLoginModule() -> LoginVC {
        let view = LoginVC()
        
        let presenter = LoginPresenter()
        let interactor: PTILoginProtocol = LoginInteractor()
        let router:PTRLoginProtocol = LoginRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
        print("nyampe router")
        return view
    }
    
    func goToHomePage(nav: UINavigationController, profileId: String) {
        //
        let vc = HomeRouter.createHomeModule()
        vc.profileId = profileId
        nav.pushViewController(vc, animated: true)
    }

}
