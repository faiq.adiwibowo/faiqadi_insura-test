//
//  LoginView.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import UIKit

class LoginVC: UIViewController {

    @IBOutlet var buttonLoginContainer: UIView!
    @IBOutlet var buttonLogin: UIButton!
    @IBOutlet var mainContainer: UIView!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var usernameField: UITextField!
    
    var presenter: VTPLoginProtocol?
    var username = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupViews()
        setupClick()
    }

    override func viewWillAppear(_ animated: Bool) {
        //
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //
    }
    private func setupViews() {
        print("nyampe view")
        buttonLoginContainer.layer.cornerRadius = 10
    }
    func setupClick(){
        buttonLogin.addTarget(self, action: #selector(onClickLogin), for: .touchUpInside)
    }
    
    @objc func onClickLogin(sender: UIButton!) {
        print("login clicked")
        username = usernameField.text!
        password = passwordField.text!
        presenter?.getLogin(nav: navigationController!, username: username, password: password)
    }
}

extension LoginVC: PTVLoginProtocol{
    func loginFailed(nav: UINavigationController) {
        //
    }
    
 
    //
    
}

