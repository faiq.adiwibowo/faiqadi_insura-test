//
//  Presenter.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import UIKit

class LoginPresenter:VTPLoginProtocol {

    var view: PTVLoginProtocol?
    var interactor: PTILoginProtocol?
    var router: PTRLoginProtocol?
    
    func getLogin(nav: UINavigationController, username: String, password: String) {
        print("Login in presenter")
        interactor?.getLogin(nav: nav, username: username, password: password)
    }
    //
}

extension LoginPresenter: ITPLoginProtocol{
    //
    func loginSuccess(nav: UINavigationController, profileId: String){
        router?.goToHomePage(nav: nav, profileId: profileId)
    }
    func loginFailed(nav: UINavigationController){
        
    }
}
