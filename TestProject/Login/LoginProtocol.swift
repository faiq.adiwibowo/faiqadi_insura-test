//
//  Protocol.swift
//  TestProject
//
//  Created by faiq adi on 26/02/22.
//

import Foundation
import UIKit

protocol VTPLoginProtocol:AnyObject{
    
    var view: PTVLoginProtocol? {get set}
    var interactor: PTILoginProtocol? {get set}
    var router: PTRLoginProtocol? {get set}
    
    func getLogin(nav: UINavigationController, username: String, password: String)
}

protocol PTVLoginProtocol:AnyObject {
    func loginFailed(nav: UINavigationController)
    
}

protocol PTRLoginProtocol:AnyObject {
    static func createLoginModule()-> LoginVC
    
    func goToHomePage(nav: UINavigationController, profileId: String)
}

protocol PTILoginProtocol:AnyObject {
    var presenter:ITPLoginProtocol? {get set}
    
    func getLogin(nav: UINavigationController, username: String, password: String)
    
}

protocol ITPLoginProtocol:AnyObject {
    func loginSuccess(nav: UINavigationController, profileId: String)
    func loginFailed(nav: UINavigationController)
}


