//
//  ProfileVC.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class ProfileVC: UIViewController {
    var presenter: VTPProfileProtocol?
    
    var profileId : String = ""
    
    @IBOutlet var labelUsername : UILabel!
    @IBOutlet var labelEmail : UILabel!
    @IBOutlet var labelAddress : UILabel!
    @IBOutlet var labelPhone: UILabel!
    @IBOutlet var footer: Footer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.getDataProfile(nav: navigationController!, Id: profileId)
        // Do any additional setup after loading the view.
        setupViews()
        setupClick()
    }

    override func viewWillAppear(_ animated: Bool) {
        //
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //
    }
    private func setupViews() {
        print("nyampe view")
        
    }
    private func setupClick(){
        footer.buttonHome.addTarget(self, action: #selector(getHome), for: .touchUpInside)
    }
    @objc func getHome(sender: UIButton!) {
        print("getProfile >>>>>\(profileId)")
        
        presenter?.getHome(nav: navigationController!, profileId: profileId)
    }
}

extension ProfileVC: PTVProfileProtocol{
    func fetchDataSuccess(nav: UINavigationController, userModel: [UserModel], username: String, email: String, phone: String, address: String) {
        //
        labelUsername.text = username
        labelEmail.text = email
        labelPhone.text = phone
        labelAddress.text = address
    }
  

    
}
