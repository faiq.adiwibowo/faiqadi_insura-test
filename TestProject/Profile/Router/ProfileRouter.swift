//
//  ProfileRouter.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class ProfileRouter:PTRProfileProtocol{

    static func createProfileModule() -> ProfileVC {
        let view = ProfileVC()
        
        let presenter = ProfilePresenter()
        let interactor: PTIProfileProtocol = ProfileInteractor()
        let router:PTRProfileProtocol = ProfileRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
        print("nyampe router")
        return view
    }
    
    func goDetailPost(nav: UINavigationController, Id: String){
        let vc = DetailRouter.createDetailModule()
        vc.Id = Id
        
        nav.pushViewController(vc, animated: true)
    }
    func getHome(nav: UINavigationController, profileId: String){
        let vc = HomeRouter.createHomeModule()
        vc.profileId = profileId
        
        nav.pushViewController(vc, animated: true)
    }

}
