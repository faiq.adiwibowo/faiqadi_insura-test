//
//  ProfileInteractor.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class ProfileInteractor: PTIProfileProtocol{
    
    var presenter: ITPProfileProtocol?
    
    func fetchAuth() {
        // fetch pakai Alamofire di sini
        
    }
    
    func getDataProfile(nav: UINavigationController, Id: String) {
        var userDataModel = [UserModel]()
        var userAdressModel = [addressDetail]()
//        let paramsAccountDetail = ["username" : "dini"]
        let url = "https://jsonplaceholder.typicode.com/users/\(Id)"
        print("url >>>\(url)")
        var username = ""
        var email = ""
        var address = ""
        var phone = ""
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
                            let dataInquiry: JSON = [
                                [
                                    "name" : "\(resJSON["name"])",
                                    "username" : "\(resJSON["username"])",
                                    "email" : "\(resJSON["email"])",
                                    "phone" : "\(resJSON["phone"])"
                                    ]
                                ]
                            
                            let dataAdress: JSON = [
                                [
                                    "street" : "\(resJSON["street"])",
                                    "suite" : "\(resJSON["suite"])",
                                    "city" : "\(resJSON["email"])",
                                    "zipcode" : "\(resJSON["zipcode"])",
                                    ]
                                ]
                            print("data inquiry \(dataInquiry)")
                            print("data address \(dataAdress)")
                            let dataBody = dataInquiry.arrayValue
                            let dataAdressResponse = dataAdress.arrayValue
                            for item in dataBody {
                                userDataModel.append(UserModel(_: item))
                            }
                            for item in dataAdressResponse {
                                userAdressModel.append(addressDetail(_: item))
                            }
                            for item in userDataModel {
                                username = item.username
                                email = item.email
                                phone = item.phone
                            }
                            for item in userAdressModel {
                                address = item.city
                            }
                            print("data userAdressModel \(userAdressModel)")
                            print("data userDataModel \(userDataModel)")
                            self.presenter?.getDataSuccess(nav: nav, userModel: userDataModel, username: username, email: email, phone: phone, address: address)
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
}
