//
//  ProfileProtocol.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

protocol VTPProfileProtocol:AnyObject{
    
    var view: PTVProfileProtocol? {get set}
    var interactor: PTIProfileProtocol? {get set}
    var router: PTRProfileProtocol? {get set}
    
    func getDataProfile(nav: UINavigationController, Id: String)
    func getHome(nav: UINavigationController, profileId: String)
}

protocol PTVProfileProtocol:AnyObject {
    func fetchDataSuccess(nav: UINavigationController, userModel: [UserModel], username: String, email: String, phone: String, address: String)
}

protocol PTRProfileProtocol:AnyObject {
    static func createProfileModule()-> ProfileVC
    func goDetailPost(nav: UINavigationController, Id: String)
    func getHome(nav: UINavigationController, profileId: String)
}

protocol PTIProfileProtocol:AnyObject {
    var presenter:ITPProfileProtocol? {get set}
    
    func getDataProfile(nav: UINavigationController, Id: String)
    
}

protocol ITPProfileProtocol:AnyObject {
    func getDataSuccess(nav: UINavigationController, userModel: [UserModel], username: String, email: String, phone: String, address: String)
    func getDataFailed(nav: UINavigationController)
}
