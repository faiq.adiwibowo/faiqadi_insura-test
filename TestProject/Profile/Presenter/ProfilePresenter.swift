//
//  ProfilePresenter.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class ProfilePresenter:VTPProfileProtocol {
    var view: PTVProfileProtocol?
    var interactor: PTIProfileProtocol?
    var router: PTRProfileProtocol?
    
    func getDataProfile(nav: UINavigationController, Id:String) {
        interactor?.getDataProfile(nav: nav, Id: Id)
    }
    func getHome(nav: UINavigationController, profileId: String) {
        router?.getHome(nav: nav, profileId: profileId)
    }

}

extension ProfilePresenter: ITPProfileProtocol{
    func getDataSuccess(nav: UINavigationController, userModel: [UserModel], username: String, email: String, phone: String, address: String) {
        view?.fetchDataSuccess(nav: nav, userModel: userModel, username: username, email: email, phone: phone, address: address)
    }
    
    func getDataFailed(nav: UINavigationController) {
        //
    }
    
    //
}
