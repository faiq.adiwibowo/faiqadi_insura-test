//
//  HomeVC.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import UIKit
import SwiftyJSON
import SwiftUI

class HomeVC: UIViewController, PostFieldDelegation {
    func onSelectedPost(index: Int) {
        
        let Id = String(index)
        print("selected POST ID \(Id)")
        presenter?.goDetailPost(nav: self.navigationController!, Id: Id)
    }
    
  
    @IBOutlet var footer: Footer!
    @IBOutlet var tablePost: PostField!
    
    var presenter: VTPHomeProtocol?
    var dataModel : [PostModel]! = []
    var profileId: String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        presenter?.getDataPost(nav: navigationController!)
        setupViews()
        setupClick()
    }

    override func viewWillAppear(_ animated: Bool) {
        //
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //
    }
    private func setupViews() {
        print("nyampe view")
        tablePost.delegate = self
        
    }
    private func setupClick(){
        footer.buttonPerson.addTarget(self, action: #selector(getProfile), for: .touchUpInside)
    }
    @objc func getProfile(sender: UIButton!) {
        print("getProfile >>>>>\(profileId)")
        
        presenter?.getProfile(nav: navigationController!, profileId: profileId)
    }
}

extension HomeVC: PTVHomeProtocol{
    func fetchDataSuccess(nav: UINavigationController, postListData: [PostModel]) {
        dataModel = postListData
        tablePost.PostData = dataModel
        
    }
    
}
