//
//  HomeProtoco;.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import UIKit

protocol VTPHomeProtocol:AnyObject{
    
    var view: PTVHomeProtocol? {get set}
    var interactor: PTIHomeProtocol? {get set}
    var router: PTRHomeProtocol? {get set}
    
    func getDataPost(nav: UINavigationController)
    func goDetailPost(nav: UINavigationController, Id: String)
    func getProfile(nav: UINavigationController, profileId: String)
}

protocol PTVHomeProtocol:AnyObject {
    func fetchDataSuccess(nav: UINavigationController, postListData: [PostModel])
}

protocol PTRHomeProtocol:AnyObject {
    static func createHomeModule()-> HomeVC
    func goDetailPost(nav: UINavigationController, Id: String)
    func getProfile(nav: UINavigationController, profileId: String)
}

protocol PTIHomeProtocol:AnyObject {
    var presenter:ITPHomeProtocol? {get set}
    
    func getDataPost(nav: UINavigationController)
    
}

protocol ITPHomeProtocol:AnyObject {
    func getDataSuccess(nav: UINavigationController, postListData: [PostModel])
    func getDataFailed(nav: UINavigationController)
}
