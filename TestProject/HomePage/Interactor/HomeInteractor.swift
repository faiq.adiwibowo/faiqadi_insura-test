//
//  HomeInteractor.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class HomeInteractor: PTIHomeProtocol{
    
    var presenter: ITPHomeProtocol?
    
    func fetchAuth() {
        // fetch pakai Alamofire di sini
        
    }
    
    func getDataPost(nav: UINavigationController) {
        var postList = [PostModel]()
//        let paramsAccountDetail = ["username" : "dini"]
        let url = "https://jsonplaceholder.typicode.com/posts"
        
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
//                            print("DATA USER")
//                            print(resJSON)
                            let allUserResponse = resJSON.arrayValue
                            for item in allUserResponse {
                                postList.append(PostModel(_: item))
                            }
                            print("DATA POST")
                            print(postList)
                            self.presenter?.getDataSuccess(nav: nav, postListData: postList)
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
}


