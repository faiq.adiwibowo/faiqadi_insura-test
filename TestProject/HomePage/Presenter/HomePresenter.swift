//
//  HomePresenter.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import UIKit

class HomePresenter:VTPHomeProtocol {
    var view: PTVHomeProtocol?
    var interactor: PTIHomeProtocol?
    var router: PTRHomeProtocol?
    
    func getDataPost(nav: UINavigationController) {
        interactor?.getDataPost(nav: nav)
    }
    //
    func goDetailPost(nav: UINavigationController, Id: String) {
        router?.goDetailPost(nav: nav, Id: Id)
    }
    func getProfile(nav: UINavigationController, profileId: String){
        router?.getProfile(nav: nav, profileId: profileId)
    }
}

extension HomePresenter: ITPHomeProtocol{
    func getDataSuccess(nav: UINavigationController, postListData: [PostModel]) {
        view?.fetchDataSuccess(nav: nav, postListData: postListData)
    }
    
    func getDataFailed(nav: UINavigationController) {
        //
    }
    
    //
}
