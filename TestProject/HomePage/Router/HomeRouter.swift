//
//  HomeRouter.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import UIKit

class HomeRouter:PTRHomeProtocol{

    static func createHomeModule() -> HomeVC {
        let view = HomeVC()
        
        let presenter = HomePresenter()
        let interactor: PTIHomeProtocol = HomeInteractor()
        let router:PTRHomeProtocol = HomeRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
        print("nyampe router")
        return view
    }
    
    func goDetailPost(nav: UINavigationController, Id: String){
        let vc = DetailRouter.createDetailModule()
        vc.Id = Id
        
        nav.pushViewController(vc, animated: true)
    }
    
    func getProfile(nav: UINavigationController, profileId: String){
        let vc = ProfileRouter.createProfileModule()
        vc.profileId = profileId
        nav.pushViewController(vc, animated: true)
    }
}
