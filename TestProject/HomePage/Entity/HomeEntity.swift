//
//  HomeEntity.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//

import Foundation
import SwiftyJSON


struct PostModel {
    var userId: Int = 0
    var id: Int = 0
    var title: String = ""
    var body: String = ""
    
    init() {}

       init(_ json: JSON) {
           if let _userId = json["userId"].int{
               userId = _userId
           }
           if let _id = json["id"].int{
               id = _id
           }
           if let _title = json["title"].string{
               title = _title
           }
           if let _body = json["body"].string{
               body = _body
           }
           
       }
}
