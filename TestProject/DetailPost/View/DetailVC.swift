//
//  DetailVC.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class DetailVC: UIViewController{
    var presenter: VTPDetailProtocol?
    
    @IBOutlet var buttonBack: UIButton!
    @IBOutlet var detailContainer: UIView!
    @IBOutlet var detailTitleLabel: UILabel!
    @IBOutlet var detailBodyLabel: UILabel!
    @IBOutlet var comentField: ComentField!
    
    var Id: String = "0"
    var comentDataModel : [ComentModel]! = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.getDetail(nav: navigationController!, Id: Id)
        // Do any additional setup after loading the view.
        setupViews()
        setupClick()
    }

    override func viewWillAppear(_ animated: Bool) {
        //
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //
    }
    private func setupViews() {
        print("nyampe view")
        detailContainer.layer.cornerRadius = 10
        detailContainer.layer.borderWidth = 1
        detailContainer.layer.borderColor = UIColor(red:188/255, green:200/255, blue:231/255, alpha: 1).cgColor
    }
    func setupClick(){
        buttonBack.addTarget(self, action: #selector(backAction), for: .touchUpInside)
    }
    
    @objc func backAction() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension DetailVC: PTVDetailProtocol{
    func getComentSuccess(nav: UINavigationController, dataComent: [ComentModel]) {
                comentDataModel = dataComent
                comentField.ComentData = comentDataModel
    }
    
    func getDetailSuccess(nav: UINavigationController, dataDetailPost: [PostModel]) {
        print("sucess reach view")
        for item in dataDetailPost {
            detailTitleLabel.text = item.title
            detailBodyLabel.text = item.body
        }

        presenter?.getComent(nav: navigationController!, Id: Id)
    }
    
    func getDetailFailed(nav: UINavigationController) {
        //
    }

    
}
