//
//  DetailPresenter.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class DetailPresenter:VTPDetailProtocol {

    var view: PTVDetailProtocol?
    var interactor: PTIDetailProtocol?
    var router: PTRDetailProtocol?
    
    func getDetail(nav: UINavigationController, Id Id: String) {
        print("Detail in presenter")
        interactor?.getDetail(nav: nav, Id: Id)
    }
    //
    func getComent(nav: UINavigationController, Id: String) {
        interactor?.getComent(nav: nav, Id: Id)
    }
}

extension DetailPresenter: ITPDetailProtocol{
    func getComentSuccess(nav: UINavigationController, dataComent: [ComentModel]) {
        view?.getComentSuccess(nav: nav, dataComent: dataComent)
    }
    
    func getDetailSuccess(nav: UINavigationController, dataDetailPost: [PostModel]) {
        print("sucess reach presenter")
        view?.getDetailSuccess(nav: nav, dataDetailPost: dataDetailPost)
    }
    
    func getDetailFailed(nav: UINavigationController) {
        //
    }
  
}
