//
//  DetailRouter.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class DetailRouter:PTRDetailProtocol{

    static func createDetailModule() -> DetailVC {
        let view = DetailVC()
        
        let presenter = DetailPresenter()
        let interactor: PTIDetailProtocol = DetailInteractor()
        let router:PTRDetailProtocol = DetailRouter()
        
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        view.presenter = presenter
        print("nyampe router")
        return view
    }
    
    func goToHomePage(nav: UINavigationController) {
        //
        let vc = HomeRouter.createHomeModule()
        
        nav.pushViewController(vc, animated: true)
    }

}
