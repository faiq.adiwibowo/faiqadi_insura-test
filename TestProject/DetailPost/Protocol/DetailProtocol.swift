//
//  DetailProtocol.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

protocol VTPDetailProtocol:AnyObject{
    
    var view: PTVDetailProtocol? {get set}
    var interactor: PTIDetailProtocol? {get set}
    var router: PTRDetailProtocol? {get set}
    
    func getDetail(nav: UINavigationController, Id: String)
    func getComent(nav: UINavigationController, Id: String)
}

protocol PTVDetailProtocol:AnyObject {
    func getDetailSuccess(nav: UINavigationController, dataDetailPost: [PostModel])
    func getDetailFailed(nav: UINavigationController)
    func getComentSuccess(nav: UINavigationController, dataComent: [ComentModel])
}

protocol PTRDetailProtocol:AnyObject {
    static func createDetailModule()-> DetailVC
    
    func goToHomePage(nav: UINavigationController)
}

protocol PTIDetailProtocol:AnyObject {
    var presenter:ITPDetailProtocol? {get set}
    
    func getDetail(nav: UINavigationController, Id: String)
    func getComent(nav: UINavigationController, Id: String)
}

protocol ITPDetailProtocol:AnyObject {
    func getDetailSuccess(nav: UINavigationController, dataDetailPost: [PostModel])
    func getComentSuccess(nav: UINavigationController, dataComent: [ComentModel])
    func getDetailFailed(nav: UINavigationController)
}

