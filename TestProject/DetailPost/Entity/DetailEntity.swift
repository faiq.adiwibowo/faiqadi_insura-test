//
//  DetailEntity.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import SwiftyJSON

struct ComentModel {
    var postId: Int = 0
    var name: String = ""
    var id: Int = 0
    var email: String = ""
    var body: String = ""
    
    init() {}

       init(_ json: JSON) {
           if let _postId = json["postId"].int{
               postId = _postId
           }
           if let _name = json["name"].string{
               name = _name
           }
           if let _id = json["id"].int{
               id = _id
           }
           if let _email = json["email"].string{
               email = _email
           }
           if let _body = json["body"].string{
               body = _body
           }
           
       }
}
