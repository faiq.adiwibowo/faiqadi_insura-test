//
//  DetailInteractor.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class DetailInteractor: PTIDetailProtocol{

       
    var presenter: ITPDetailProtocol?
    
    func getDetail(nav: UINavigationController, Id: String) {
        //hit api here
        let url = "https://jsonplaceholder.typicode.com/posts/\(Id)"
        print("DATA URL  \(url)")
        var dataDetail = [PostModel]()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
                            
                            
                            let dataInquiry: JSON = [
                                [
                                        "userId" : "\(resJSON["adminFee"])",
                                        "id" : "\(resJSON["id"])",
                                        "title" : "\(resJSON["title"])",
                                        "body" : "\(resJSON["body"])"
                                    ]
                                ]
                           
                            let dataBody = dataInquiry.arrayValue
                            let dataTitle = dataInquiry["title"]
                            for item in dataBody {
                                dataDetail.append(PostModel(_: item))
                            }
                            
                            print("DATA DETAIL \(dataDetail)")
                            self.presenter?.getDetailSuccess(nav: nav, dataDetailPost: dataDetail )
                        case .failure(let error):
                            print(error)
                        }
                    })
    }
    
    func getComent(nav: UINavigationController, Id: String) {
        //hit api here
        let url = "https://jsonplaceholder.typicode.com/posts/\(Id)/comments"
        print("DATA URL  \(url)")
        var comentDetail = [ComentModel]()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
                            let dataComent = resJSON.arrayValue
                            for item in dataComent {
                                comentDetail.append(ComentModel(_: item))
                            }

                            print("DATA dataComent \(dataComent)")
                            self.presenter?.getComentSuccess(nav: nav, dataComent: comentDetail)
                        case .failure(let error):
                            print(error)
                        }
                    })
    }


    
    
}
