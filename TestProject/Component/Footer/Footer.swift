//
//  Footer.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit

class Footer: UIView {
    
    
    @IBOutlet var buttonPerson: UIButton!
    @IBOutlet var buttonHome: UIButton!
    let nibName = "Footer"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        guard let view = self.loadViewFromNib(nibName: nibName) else{ return }
        view.frame = self.bounds
        setupView()
        self.addSubview(view)
    }
    
    func loadViewFromNib(nibName: String) -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    
    func setupView() {
        //
   }
}
