//
//  PostCell.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelBody: UILabel!
    @IBOutlet var container: UIView!
    @IBOutlet var totalComentLabel: UILabel!
    
    static let identifier = "PostCellIdentifier"
    static func nib() -> UINib {
        return UINib(nibName: "PostCell", bundle: nil)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = UIColor.clear
        container.layer.cornerRadius = 10
        container.layer.borderWidth = 1
        container.layer.borderColor = UIColor(red:188/255, green:200/255, blue:231/255, alpha: 1).cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
//        container.layer.cornerRadius = 10
//        container.layer.borderWidth = 1
//        container.layer.borderColor = UIColor(red:188/255, green:200/255, blue:231/255, alpha: 1).cgColor

        // Configure the view for the selected state
    }
    
}
