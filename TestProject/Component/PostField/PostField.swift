//
//  PostField.swift
//  TestProject
//
//  Created by faiq adi on 27/02/22.
//


import Foundation
import UIKit
import SwiftyJSON
import SwiftUI
import Alamofire

protocol PostFieldDelegation:NSObjectProtocol {
    func onSelectedPost(index: Int)
}

class PostField: UIView, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var PostFieldTable: UITableView!
    
//    var historySelected: PostModel!
    var PostData: [PostModel] = [PostModel]() {
        didSet {
            PostFieldTable.reloadData()
        }
    }
    
    weak var delegate: PostFieldDelegation?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: "PostField", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        setupView()
        self.addSubview(view)
    }
    
    func setupView() {
        let nib = UINib(nibName: "PostCell", bundle: nil)
        PostFieldTable.register(nib, forCellReuseIdentifier: "PostCellIdentifier")
        PostFieldTable.dataSource = self
        PostFieldTable.delegate = self
//        PostFieldTable.isScrollEnabled = false
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PostData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        
        // convert Int to TimeInterval (typealias for Double)
        
        
        let PostCellIdentifier = PostFieldTable.dequeueReusableCell(withIdentifier: "PostCellIdentifier", for: indexPath) as! PostCell
        
        PostCellIdentifier.labelTitle.text = PostData[indexPath.row].title
        PostCellIdentifier.labelBody.text = PostData[indexPath.row].body
        
        PostCellIdentifier.selectedBackgroundView = bgColorView
        self.PostFieldTable.bounces = true;
        
//MARK: hit API to get total coment for each data
        let url = "https://jsonplaceholder.typicode.com/posts/\(PostData[indexPath.row].id)/comments/"
        var dataComent = [ComentModel]()
        AF.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil)
                    .responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            let resJSON = JSON(response.value!)
                            let dataComentResponse = resJSON.arrayValue
                            for item in dataComentResponse {
                                dataComent.append(ComentModel(_: item))
                            }
                            PostCellIdentifier.totalComentLabel.text = String(dataComent.count)
                        case .failure(let error):
                            print(error)
                        }
                    })
        
        
        return PostCellIdentifier
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let Id = PostData[indexPath.row].id
        print("selected cell value ID \(Id)")
        delegate?.onSelectedPost(index: Id)
    }
}
