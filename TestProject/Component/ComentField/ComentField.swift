//
//  ComentFielf.swift
//  TestProject
//
//  Created by faiq adi on 28/02/22.
//

import Foundation
import UIKit
import SwiftyJSON
import SwiftUI



class ComentField: UIView, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var ComentFieldTable: UITableView!
    
//    var historySelected: ComentModel!
    var ComentData: [ComentModel] = [ComentModel]() {
        didSet {
            ComentFieldTable.reloadData()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func loadViewFromNib() -> UIView? {
        let nib = UINib(nibName: "ComentField", bundle: nil)
        return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
    
    func commonInit() {
        guard let view = loadViewFromNib() else { return }
        view.frame = self.bounds
        setupView()
        self.addSubview(view)
    }
    
    func setupView() {
        let nib = UINib(nibName: "ComentFieldCell", bundle: nil)
        ComentFieldTable.register(nib, forCellReuseIdentifier: "ComentCellIdentifier")
        ComentFieldTable.dataSource = self
        ComentFieldTable.delegate = self
        ComentFieldTable.isScrollEnabled = false
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ComentData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        
        // convert Int to TimeInterval (typealias for Double)
        
        
        let ComentCellIdentifier = ComentFieldTable.dequeueReusableCell(withIdentifier: "ComentCellIdentifier", for: indexPath) as! ComentFieldCell
        
        ComentCellIdentifier.nameLabel.text = ComentData[indexPath.row].name
        ComentCellIdentifier.bodyLabel.text = ComentData[indexPath.row].body
        
        ComentCellIdentifier.selectedBackgroundView = bgColorView
        self.ComentFieldTable.bounces = true;
        
        return ComentCellIdentifier
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected cell \(indexPath.row)")
    }
}
