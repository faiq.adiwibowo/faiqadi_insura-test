//
//  ComentFieldCell.swift
//  TestProject
//
//  Created by faiq adi on 01/03/22.
//

import UIKit

class ComentFieldCell: UITableViewCell {

    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var bodyLabel: UILabel!
    
    static let identifier = "ComentCellIdentifier"
    static func nib() -> UINib {
        return UINib(nibName: "ComentFieldCell", bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
